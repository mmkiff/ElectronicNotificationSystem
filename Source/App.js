'use strict';
{
	let Board = require('johnny-five').Board;
	let NotificationSystem = require('./NotificationSystem').NotificationSystem;
	let Hardware = require('./Hardware').Hardware;	
	let Server = require('./Server').WebUI;
			
	let arduino = new Board();	
	let webUI = new Server();		
	
	arduino.on("ready", function() {
		let electronics = new Hardware();
		let notificationSystem = new NotificationSystem(electronics, webUI);
	
		this.repl.inject({
			matrix: electronics.matrix,
			rgb1: electronics.rgb1,
			rgb2: electronics.rgb2,
			led1: electronics.led1,
			led2: electronics.led2,
			led3: electronics.led3,
			led4: electronics.led4,
			webUI: webUI,
			notificationSystem: notificationSystem
		});
		
		notificationSystem.start();
	});		
}
'use strict';
{
	let express = require('express');
	let bodyParser = require('body-parser');
	let http = require('http');
	let socketIo = require('socket.io');
	
	class WebUI {
		constructor() {
			let port = 3000;
			let app = express();
			let server = http.Server(app);			
			this.messageReceivedEvent = null;
			this.io = socketIo(server);			
			
			this.logs = {}; // TO DO - move logs out of server
			
			app.use(this.addHeaders);
			app.use(bodyParser.json());
			app.use(express.static(__dirname + '/Public'));			
			
			app.get('/', this.home);
			app.post('/message', (req, res) => { this.message(req, res); });

			this.io.on('connection', (socket) => {
				this.log(null, null, 'system');
				this.log(null, null, 'LEDs');
				this.log(null, null, 'RGB1');
				this.log(null, null, 'RGB2');

				this.io.emit('messageFromServer', '');	// TO DO - rename
				
				socket.on('disconnect', function(){
					console.log('A client disconnected.');
				});
				
				socket.on('messageFromClient', function(val){
					// a client has connected.
					console.log('A client has connected.');
				});
			});			

			server.listen(port, () => {
				console.log(`listening on port ${port}...`);
			});		
		}

		addHeaders(req, res, next) {
			res.header('Access-Control-Allow-Origin', '*');
			res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
			next();
		}
		
		home(req, res) {
			//res.send('home');
			res.sendFile(__dirname + '/index.htm');
		}
		
		message(req, res) {			
			let message = {
				from: req.body.from,
				to: req.body.to,
				key: req.body.key,
				value: req.body.value
			};
			
			let result = null;
			if(this.messageReceivedEvent != null)
				result = this.messageReceivedEvent(message);
			else
				this.log('No message received handler found.', 'error', 'system');
			
			res.send(result);
		}
		
		log(message, type, source) {
			if(this.logs[source] == undefined)
				this.logs[source] = [];
			
			if(message != null) {
				console.log(source + ': ' + message);
				this.logs[source].unshift({message: message, type: type});
				
				if(this.logs[source].length > 50)
					this.logs[source].pop();
			}
			
			this.io.emit('messageLogged', { 
				source: source,
				messages: this.logs[source]
			});
		}
	}
	
	exports.WebUI = WebUI;
}
'use strict';
{
	let five = require('johnny-five');	

	class Hardware {
		constructor() {
			this.matrix = null;
			this.rgb1 = null;
			this.rgb2 = null;
			this.led1 = null;
			this.led2 = null;
			this.led3 = null;
			this.led4 = null;
			
			this.initialize();
		}
		
		initialize() {
			this.matrix = new five.Led.Matrix({
				pins: {
					data: 2,
					clock: 12, // This was originally using a PWM pin (3), but seems fine on 12.
					cs: 4
				}
			});
			
			this.rgb1 = new five.Led.RGB({
				pins: {
					red: 5,
					green: 9,
					blue: 6
				}
			});
			
			this.rgb2 = new five.Led.RGB({
				pins: {
					red: 10,
					green: 3, 
					blue: 11
				}
			});
			
			this.led1 = new five.Led('A2');
			this.led2= new five.Led('13');
			this.led3 = new five.Led(8);
			this.led4 = new five.Led(7);
			
			let pattern = [
				'00000000',
				'01111110',
				'01000010',
				'01011010',
				'01011010',
				'01000010',
				'01111110',
				'00000000'
			];

			this.matrix.draw(pattern);
			
			this.rgb1.intensity(5);
			this.rgb2.intensity(5);
			
			this.rgb1.color('#FFFFFF');
			this.rgb2.color('#FFFFFF');

			this.rgb1.off();
			this.rgb2.off();
		}
	}
	
	exports.Hardware = Hardware;
}
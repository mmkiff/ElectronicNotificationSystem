"use strict";
{
	let rgbNotification = require('./RGBNotification');
	
	class TestColorChanger extends rgbNotification.RGBNotification {
		constructor(rgb, id, logger) {
			let frequency = 3000;
			let name = 'Color Changer';
			let description = `Cycles colors for debugging.`;
			let config = {};
			super(rgb, id, frequency, logger, name, description, config);
			
			let isOn = false;
			this.rgb.color('#000000');
			this.rgb.on();
			this.colors = ['#FF0000', '#00FF00', '#0000FF'];
			this.colorIndex = 0;
		}
		
		action() {
			super.log('Changing color.');
			this.colorIndex++;
			if(this.colorIndex > 2)
				this.colorIndex = 0;
			
			this.rgb.color(this.colors[this.colorIndex]);
		}
	}
	
	exports.TestColorChanger = TestColorChanger;
}

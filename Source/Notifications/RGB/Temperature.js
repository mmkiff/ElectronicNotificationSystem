"use strict";
{
	let rgbNotification = require('./RGBNotification');
	let request = require('request');
	let cheerio = require('cheerio');
	
	class Temperature extends rgbNotification.RGBNotification {
		constructor(rgb, id, logger) {
			let frequency = 600000;	// 10 minutes	
			let name = 'Temperature';
			let description = `LED turns brighter or fainter depending on how close it is to the specified value. Uses the absolute temperature value, and displays red if > 0, blue if < 0, and white if 0.`;
			let config = {
				weatherUndergroundAPIKey: {
					name: 'API key for wunderground.com',
					value: ''
				},
				pws: {
					name: 'Weather station pws',
					value: 'IABCALGA305'
				},
				maxTemperature: {
					name: 'Maximum brightness at this temperature',
					value: 25
				}
			};		
			super(rgb, id, frequency, logger, name, description, config);
		}
		
		getJson(url) { // TO DO - this is going to be reused a lot and i need to move it somewhere. seems like something that should be built into the request library... check if it is.
			return new Promise((resolve, reject) => {				
				super.log('getting url: ' + url);
				
				request(url, function (error, response, body) {
					if (!error) {
						resolve(JSON.parse(body));
					} 
					else {
						reject(error);
					}
				});		
			});
		}
		
		action() {
			if(this.config.weatherUndergroundAPIKey.value == '') {
				super.log('No API key specified. Please edit configuration.');
				return;
			}
			
			let url = `http://api.wunderground.com/api/${this.config.weatherUndergroundAPIKey.value}/geolookup/conditions/q/pws:${this.config.pws.value}.json`;
			
			super.log('Checking temperature.' + url);
			
			let response = this.getJson(url);
			response.then((data) => {
				this.rgb.on();
				
				let temperature = Math.round(data.current_observation.temp_c);

				super.log('Temperature: ' + temperature);					 
				
				let intensity = Math.abs(temperature / this.config.maxTemperature.value) * 255;
				super.log('Setting intensity to ' + intensity);
				
				if(temperature > 0)
					this.rgb.color(intensity, 0, 0);
				else if(temperature < 0)
					this.rgb.color(0, 0, intensity);
				else
					this.rgb.color('#FFFFFF');
			})
			.catch((reason) => {
				super.log('error: ' + reason)
				this.rgb.color('#00FF00'); // TO DO - blink
			});
		}		
	}
	
	exports.Temperature = Temperature;
}

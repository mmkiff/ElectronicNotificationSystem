"use strict";
{
	let rgbNotification = require('./RGBNotification');
	
	class WebControlled extends rgbNotification.RGBNotification {
		constructor(rgb, id, logger) {
			let frequency = 120 * 60 * 1000; // 2 hours	// TO DO - make a function that takes minutes -> milliseconds
			let name = 'Web Controlled';
			let description = `Listens for messages via web service. Valid commands are color (hex value), and state (either 1 or 0).`;
			let config = {};
			super(rgb, id, frequency, logger, name, description, config);
		}
		
		action() {
			super.log('Waiting for message.');
		}	

		messageReceivedFromWeb(message) {
			let result = null;
			
			if(message.to == 'WebControlled') {
				let success = true;
				super.log('Handling message: ' + message);
				if(message.key == 'color')
					this.color(message);
				else if(message.key == 'state')
					this.state(message);
				else {
					super.log('unknown key: ' + message.key);
					success = false;
				}
				result = success;
			}
			else
				result = super.messageReceivedFromWeb(message);
		
			return result;
		}

		color(message) { // TO DO - validation
			let hexColor = '#' + message.value;
			super.log('Setting color to ' + hexColor);
			
			this.rgb.color(hexColor);
			this.rgb.on();
		}
		
		state(message) { // TO DO - validation
			let state = message.value;
			super.log('Setting state to ' + state);

			if(state == 0)
				this.rgb.off();
			else
				this.rgb.on();
		}
	}
	
	exports.WebControlled = WebControlled;
}

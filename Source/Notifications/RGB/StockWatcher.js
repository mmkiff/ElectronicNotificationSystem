"use strict";
{
	let rgbNotification = require('./RGBNotification');
	let request = require('request');
	let cheerio = require('cheerio');
	
	class StockWatcher extends rgbNotification.RGBNotification {
		constructor(rgb, id, logger) {
			let frequency = 60 * 60 * 1000;	// 60 minutes		
			let name = 'Stock Watcher'
			let description = `Monitors a specified stock. Sets LED intensity based off how close the price is to a designated target.`;
			let config = {
				url: {
					name: 'Marketwatch.com address',
					value: 'http://www.marketwatch.com/investing/stock/NRN?countrycode=CA'
				},
				targetPrice: {
					name: 'Maximum brightness price',
					value: 5
				}
			};
			super(rgb, id, frequency, logger, name, description, config);
		}
		
		getBody(url) { // TO DO - this is going to be reused a lot and i need to move it somewhere. seems like something that should be built into the request library... check if it is.
			return new Promise((resolve, reject) => {				
				super.log('getting url: ' + url);
				
				request(url, function (error, response, body) {
					if (!error) {
						let $ = cheerio.load(body);						
						resolve($);
					} 
					else {
						reject(error);
					}
				});		
			});
		}
		
		action() {
			super.log('Checking stock: ' + this.config.url.value);
			
			let body = this.getBody(this.config.url.value);
			body.then(($) => {
				this.rgb.on();
				
				let currentPrice = Number($('.lastprice .data.bgLast').text().trim());

				super.log('Current value: ' + currentPrice);		
				
				if(currentPrice < 0.08)
					this.rgb.color("#FF0000");
				else {
					let intensity = Math.abs(currentPrice / this.config.targetPrice.value) * 255;
					super.log('Setting intensity to ' + intensity);
					this.rgb.color(0, intensity, 0);
				}				
			})
			.catch((reason) => {
				super.log('error: ' + reason)
				this.rgb.color('#00FF00'); // TO DO - blink
			});
		}		
	}
	
	exports.StockWatcher = StockWatcher;
}

"use strict";
{
	let fs = require('fs');
	
	class RGBNotification {
		constructor(rgb, id, frequency, webUI, name, description, config) {
			this.rgb = rgb;
			this.id = id;
			this.frequency = frequency;
			this.webUI = webUI; // TO DO - instead of passing the webUI it would be nicer to either just pass the function (but then it had the wrong 'this') or make a different logger class to pass.
			this.config = config;
			this.name = name;
			this.description = description;
			this.intervalId = null;
			
			if(fs.existsSync(this.getConfigLocation()))
				this.loadConfig();
			else {
				this.saveConfig();
				this.log('Creating config file: ' + this.getConfigLocation());
			}
		}
		
		start() {
			this.runAction();
			this.intervalId = setInterval(() => { this.runAction(); }, this.frequency);
			console.log('started interval id ' + this.intervalId);
		}
		
		stop() {
			if(this.intervalId != null) {
				clearInterval(this.intervalId);
				console.log('STOPPING RGB ' + this.id);
			}
			else
				console.log('no interval id found.');
		}
		
		runAction() {
			try {
				this.action();
			}
			catch(err) {
				this.log('Error: ' + err);
			}
		}
		
		action() {
			console.log('no action defined.');
			this.blink();
		}
		
		log(message, type = 'Info') {
			this.webUI.log(message, type, 'RGB' + this.id);
		}
		
		messageReceivedFromWeb(message) {
			let result = null;
			
			if(message.to == 'RGB' + this.id) {				
				if(message.key == 'getConfig')
					result = this.config;
				else if(message.key == 'setConfig') {
					this.config = message.value;
					this.saveConfig();
					this.action();
					result = { success: true };
				}
				else if(message.key == 'info')
					result = { name: this.name, description: this.description };
				else
					result = "Unknown command";
			}
		
			return result;
		}
		
		getConfigLocation() {
			return `./Notifications/RGB/${this.name}.config`;
		}
		
		saveConfig() {
			fs.writeFileSync(this.getConfigLocation(), JSON.stringify(this.config), 'utf-8');
		}
		
		loadConfig() {
			let config = fs.readFileSync(this.getConfigLocation(), 'utf-8');
			this.config = JSON.parse(config);
		}
	}
	
	exports.RGBNotification = RGBNotification;
}

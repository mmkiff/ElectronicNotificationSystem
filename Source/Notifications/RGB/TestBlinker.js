"use strict";
{
	let rgbNotification = require('./RGBNotification');
	
	class TestBlinker extends rgbNotification.RGBNotification {
		constructor(rgb, id, logger) {
			let frequency = 3000;
			let name = 'Blinker';
			let description = `Blinks the led on and off for debugging.`;
			let config = {};
			super(rgb, id, frequency, logger, name, description, config);
			
			let isOn = false;
			this.rgb.color('#0000ff');
		}
		
		action() {
			super.log('Toggling.');
			this.isOn = !this.isOn;
			if(this.isOn)
				this.rgb.on();
			else
				this.rgb.off();
		}
	}
	
	exports.TestBlinker = TestBlinker;
}

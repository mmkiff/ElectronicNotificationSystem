"use strict";
{
	let matrixNotification = require('./MatrixNotification');
	let request = require('request');
	let cheerio = require('cheerio');
	
	class DrawTest extends matrixNotification.MatrixNotification {
		constructor(matrix, logger) {
			var frequency = 10 * 60 * 1000;	// 10 minutes	
			let name = 'Draw Test';
			let description = `Draws a pattern.`;
			let config = {
				pattern: {
					name: 'Pattern to draw',
					value: [
						'10000000',
						'00000010',
						'00001111',
						'00001010',
						'00001000',
						'00001000',
						'00001000',
						'00001001'
					]
				},
				offsetX: {
					name: 'X Offset',
					value: 0
				},
				offsetY: {
					name: 'Y Offset',
					value: 0 // TO DO - make sure changing this from the UI actually works
				}
			};				
			super(matrix, frequency, logger, name, description, config);
			
			this.matrix.clear();
		}
		
		action() {
			this.draw(this.config.pattern.value, this.config.offsetX.value, this.config.offsetY.value);
		}		
	}
	
	exports.DrawTest = DrawTest;
}

"use strict";
{
	let fs = require('fs');
	
	class MatrixNotification {
		constructor(matrix, frequency, webUI, name, description, config) {
			this.matrix = matrix;
			this.frequency = frequency;
			this.webUI = webUI; // TO DO - instead of passing the webUI it would be nicer to either just pass the function (but then it had the wrong 'this') or make a different logger class to pass.
			this.config = config;
			this.name = name;
			this.description = description;
			this.intervalId = null;
			
			if(fs.existsSync(this.getConfigLocation()))
				this.loadConfig();
			else {
				this.saveConfig();
				this.log('Creating config file: ' + this.getConfigLocation());
			}
			
			this.numberPatterns = {}; // TO DO - move these to a seperate file... character patterns instead of number patterns
			
			this.numberPatterns['0'] = [
				'111',
				'101',
				'101',
				'101',
				'111'
			];
			
			this.numberPatterns['1'] = [
				'010',
				'010',
				'010',
				'010',
				'010'
			];
			
			this.numberPatterns['2'] = [
				'111',
				'001',
				'111',
				'100',
				'111'
			];
			
			this.numberPatterns['3'] = [
				'111',
				'001',
				'111',
				'001',
				'111'
			];
			
			this.numberPatterns['4'] = [
				'101',
				'101',
				'111',
				'001',
				'001'
			];
			
			this.numberPatterns['5'] = [
				'111',
				'100',
				'111',
				'001',
				'111'
			];
			
			this.numberPatterns['6'] = [
				'111',
				'100',
				'111',
				'101',
				'111'
			];
			
			this.numberPatterns['7'] = [
				'111',
				'001',
				'001',
				'001',
				'001'
			];
			
			this.numberPatterns['8'] = [
				'111',
				'101',
				'111',
				'101',
				'111'
			];
			
			this.numberPatterns['9'] = [
				'111',
				'101',
				'111',
				'001',
				'001'
			];
		}
		
		start() {
			this.runAction();
			this.intervalId = setInterval(() => { this.runAction(); }, this.frequency);
		}
		
		stop() {
			if(this.intervalId != null)
				clearInterval(this.intervalId);
		}
		
		runAction() {
			try {
				this.action();
			}
			catch(err) {
				this.log('Error: ' + err);
			}
		}
		
		action() {
			console.log('no action defined.');
			this.blink();
		}
		
		draw(pattern, startX = 0, startY = 0) {
			for(let row = 0; row < pattern.length; row++) {
				for(let column = 0; column < pattern[row].length; column++) {
					this.drawPoint(column + startX, row + startY, Number(pattern[row][column]));
				}
			}
		}
		
		drawPoint(x, y, value) {
			this.matrix.led(x, 7 - y, value);
		}
		
		drawNumber(number) { // TO DO - make 1 look nicer
			let numberString = number + '';
			this.matrix.clear();
			
			let isNegative = number < 0;
			if(isNegative) {
				numberString = numberString.replace('-', '');
				this.drawPoint(0, 3, 1);
			}
			
			if(numberString.length == 1) {
				this.draw(this.numberPatterns[numberString], 3, 1);
			}
			else if(numberString.length == 2) {
				this.draw(this.numberPatterns[numberString[0]], 1, 1);
				this.draw(this.numberPatterns[numberString[1]], 5, 1);
			}
			else
				console.log('Number is too big.');
		}
		
		log(message, type = 'Info') {
			this.webUI.log(message, type, 'Matrix');
		}
		
		messageReceivedFromWeb(message) {
			let result = null;
			
			if(message.to == 'Matrix') {				
				if(message.key == 'getConfig')
					result = this.config;
				else if(message.key == 'setConfig') {
					this.config = message.value;
					this.saveConfig();
					this.action();
					result = { success: true };
				}
				else if(message.key == 'info')
					result = { name: this.name, description: this.description };
				else
					result = "Unknown command";
			}
		
			return result;
		}
		
		getConfigLocation() {
			return `./Notifications/Matrix/${this.name}.config`;
		}
		
		saveConfig() {
			fs.writeFileSync(this.getConfigLocation(), JSON.stringify(this.config), 'utf-8');
		}
		
		loadConfig() {
			let config = fs.readFileSync(this.getConfigLocation(), 'utf-8');
			this.config = JSON.parse(config);
		}
	}
	
	exports.MatrixNotification = MatrixNotification;
}

"use strict";
{
	let matrixNotification = require('./MatrixNotification');
	let request = require('request');
	let cheerio = require('cheerio');
	
	class Temperature extends matrixNotification.MatrixNotification {
		constructor(matrix, logger) {
			var frequency = 10 * 60 * 1000;	// 10 minutes	
			let name = 'Temperature';
			let description = `Displays the current temperature.`;
			let config = {
				weatherUndergroundAPIKey: {
					name: 'API key for wunderground.com',
					value: ''
				},
				pws: {
					name: 'Weather station pws',
					value: 'IABCALGA305'
				}
			};					
			super(matrix, frequency, logger, name, description, config);
			
			this.previousTemperature = -99;
			this.matrix.clear();
		}
		
		getJson(url) { // TO DO - this is going to be reused a lot and i need to move it somewhere. seems like something that should be built into the request library... check if it is.
			return new Promise((resolve, reject) => {				
				super.log('getting url: ' + url);
				
				request(url, function (error, response, body) {
					if (!error) {
						resolve(JSON.parse(body));
					} 
					else {
						reject(error);
					}
				});		
			});
		}
		 // TO DO - store the current temp and only update if it's different to avoid screen flickering
		action() {
			if(this.config.weatherUndergroundAPIKey.value == '') {
				super.log('No API key specified. Please edit configuration.');
				return;
			}
			
			let url = `http://api.wunderground.com/api/${this.config.weatherUndergroundAPIKey.value}/geolookup/conditions/q/pws:${this.config.pws.value}.json`; // Calgary
			
			super.log('Checking temperature.' + url);
			
			let response = this.getJson(url);
			response.then((data) => {
				let temperature = Math.round(data.current_observation.temp_c);

				super.log('Temperature: ' + temperature);
				if(this.previousTemperature != temperature) {
					super.drawNumber(temperature);
					this.previousTemperature = temperature;
				}
			})
			.catch((reason) => {
				super.log('error: ' + reason)
				this.rgb.color('#00FF00'); // TO DO - blink
			});			
		}		
	}
	
	exports.Temperature = Temperature;
}

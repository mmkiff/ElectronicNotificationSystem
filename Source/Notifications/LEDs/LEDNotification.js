"use strict";
{
	let fs = require('fs');
	
	class LEDNotification {
		constructor(led1, led2, led3, led4, frequency, webUI, name, description, config) {
			this.led1 = led1;
			this.led2 = led2;
			this.led3 = led3;
			this.led4 = led4; 
			this.frequency = frequency;
			this.webUI = webUI; // TO DO - instead of passing the webUI it would be nicer to either just pass the function (but then it had the wrong 'this') or make a different logger class to pass.
			this.config = config;
			this.name = name;
			this.description = description;
			this.intervalId = null;
			
			if(fs.existsSync(this.getConfigLocation()))
				this.loadConfig();
			else {
				this.saveConfig();
				this.log('Creating config file: ' + this.getConfigLocation());
			}
		}
		
		start() {
			this.runAction();
			this.intervalId = setInterval(() => { this.runAction(); }, this.frequency);
		}
		
		stop() {
			if(this.intervalId != null)
				clearInterval(this.intervalId);
		}
		
		runAction() {
			try {
				this.action();
			}
			catch(err) {
				this.log('Error: ' + err);
			}
		}
		
		action() {
			console.log('no action defined.');
			this.blinkLEDs();
		}
		
		log(message, type = 'Info') {
			this.webUI.log(message, type, 'LEDs');
		}
		
		setLEDs(led1State, led2State, led3State, led4State) {
			this.stopBlinking();
			
			if(led1State)
				this.led1.on();
			else
				this.led1.off();
			
			if(led2State)
				this.led2.on();
			else
				this.led2.off();
			
			if(led3State)
				this.led3.on();
			else
				this.led3.off();
			
			if(led4State)
				this.led4.on();
			else
				this.led4.off();
		}
		
		blinkLEDs() {
			this.led1.blink(1000);
			this.led2.blink(1000);
			this.led3.blink(1000);
			this.led4.blink(1000);
		}
		
		stopBlinking() {
			// NOTE: This doesn't necessarily turn it off. It stops in whatever state it was in.
			this.led1.stop();
			this.led2.stop();
			this.led3.stop();
			this.led4.stop();			
		}
		
		messageReceivedFromWeb(message) {
			let result = null;
			
			if(message.to == 'LEDs') {				
				if(message.key == 'getConfig')
					result = this.config;
				else if(message.key == 'setConfig') {
					this.config = message.value;
					this.saveConfig();
					this.action();
					result = { success: true };
				}
				else if(message.key == 'info')
					result = { name: this.name, description: this.description };
				else
					result = "Unknown command";
			}
		
			return result;
		}
		
		getConfigLocation() {
			return `./Notifications/LEDs/${this.name}.config`;
		}
		
		saveConfig() {
			fs.writeFileSync(this.getConfigLocation(), JSON.stringify(this.config), 'utf-8');
		}
		
		loadConfig() {
			let config = fs.readFileSync(this.getConfigLocation(), 'utf-8');
			this.config = JSON.parse(config);
		}
	}
	
	exports.LEDNotification = LEDNotification;
}

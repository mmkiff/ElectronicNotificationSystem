"use strict";
{
	let ledNotifications = require('./LEDNotification');
	let request = require('request');
	let cheerio = require('cheerio');
	
	class Weather extends ledNotifications.LEDNotification {
		constructor(led1, led2, led3, led4, logger) {
			var frequency = 900000;	// 15 minutes		
			let name = 'Weather';
			let description = `Displays the current weather.`;
			let config = {
				weatherUndergroundAPIKey: {
					name: 'API key for wunderground.com',
					value: ''
				},
				pws: {
					name: 'Weather station pws',
					value: 'IABCALGA305'
				}
			};	
			super(led1, led2, led3, led4, frequency, logger, name, description, config);
			
			this.cloudyConditions = ['haze', 'overcast', 'partly cloudy', 'cloudy', 'mostly cloudy', 'mist', 'freezing fog'];
			this.sunnyConditions = ['clear', 'scattered clouds'];
			this.rainyConditions = ['light rain mist', 'thunderstorm', 'rain', 'rain mist', 'light showers rain'];
			this.snowyConditions = ['snow showers', 'snow', 'light snow mist', 'light snow'];
		}
		
		getJson(url) { // TO DO - this is going to be reused a lot and i need to move it somewhere. seems like something that should be built into the request library... check if it is.
			return new Promise((resolve, reject) => {				
				request(url, function (error, response, body) {
					try {
						if (!error) {
							resolve(JSON.parse(body));
						} 
						else {
							reject(error);
						}
					}
					catch(err) {
						reject(err);
					}
				});		
			});
		}
		
		action() {
			if(this.config.weatherUndergroundAPIKey.value == '') {
				super.log('No API key specified. Please edit configuration.');
				return;
			} 
			
			let url = `http://api.wunderground.com/api/${this.config.weatherUndergroundAPIKey.value}/geolookup/conditions/q/pws:${this.config.pws.value}.json`;
			
			super.log('Checking weather conditions.');
			
			let response = this.getJson(url);
			response.then((data) => {
				try {
					let currentConditions = data.current_observation.weather.toLowerCase(); 

					super.log('Current conditions: ' + currentConditions);					 
					
					if(this.sunnyConditions.includes(currentConditions)) {
						super.log(`It's sunny (${currentConditions}).`);
						super.setLEDs(1, 0, 0, 0);
					}
					else if(this.cloudyConditions.includes(currentConditions)) {
						super.log(`It's cloudy (${currentConditions}).`);
						super.setLEDs(0, 1, 0, 0);
					}
					else if(this.rainyConditions.includes(currentConditions)) {
						super.log(`It's rainy (${currentConditions}).`);
						super.setLEDs(0, 0, 1, 0);
					}
					else if(this.snowyConditions.includes(currentConditions)) {
						super.log(`It's snowy (${currentConditions}).`);
						super.setLEDs(0, 0, 0, 1);
					}
					else {
						super.log('Unknown conditions: ' + currentConditions);
						super.setLEDs(0, 0, 0, 0);
					}
				}
				catch(err) {
					super.log('Error: ' + err);
				}
			})
			.catch((reason) => {
				super.log('Error: ' + reason);
				super.blinkLEDs();
			});
		}		
	}
	
	exports.Weather = Weather;
}

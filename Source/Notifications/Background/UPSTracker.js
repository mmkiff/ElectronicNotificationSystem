"use strict";
{
	let backgroundNotification = require('./BackgroundNotification');
	let request = require('request');
	let cheerio = require('cheerio');
	let notifier = require('node-notifier');
	let nodemailer = require('nodemailer');
	let smtpTransport = require('nodemailer-smtp-transport');
	
	class UPSTracker extends backgroundNotification.BackgroundNotification {
		constructor(logger) {
			let previousTrackingRecords = 0; // TO DO - save this between runs. might be able to just stick it in the config
			
			let frequency = 30 * 60 * 1000;	// 30 minutes		
			let name = 'UPS Tracker'
			let description = `Monitors a given UPS tracking link for changes.`;
			let config = {
				url: {
					name: 'Tracking url',
					value: ''
				},
				emailFrom: {
					name: 'Gmail account to email From',
					value: ''
				},
				emailTo: {
					name: 'Email To',
					value: ''
				},
				emailPassword: {
					name: 'Email Password',
					value: ''
				}
			};
			super(frequency, logger, name, description, config);
		}
		
		getBody(url) { // TO DO - this is going to be reused a lot and i need to move it somewhere. seems like something that should be built into the request library... check if it is.
			return new Promise((resolve, reject) => {				
				super.log('getting url: ' + url);
				
				request(url, function (error, response, body) {
					if (!error) {
						let $ = cheerio.load(body);						
						resolve($);
					} 
					else {
						reject(error);
					}
				});		
			});
		}
		
		getCellValue($, row, index) {
			let cell = row.find('td')[index];
			let result = $(cell).text().replace(/\s+/g, ' ').trim();
			
			return result;
		}
		
		alert(result) {
			let message = `Last update shows package in ${result.location} for ${result.activity} at ${result.date} ${result.time}.`;
			super.log(message);
			
			notifier.notify({
			  title: 'UPS package is on the move!',
			  message: message,
			  sound: true,
			  wait: true 
			});
			
			notifier.on('click', function (notifierObject, options) {
				open(this.config.url.value);
			});

			let transport = nodemailer.createTransport(smtpTransport({
				service: 'gmail',
				auth: {
					user: this.config.emailFrom.value, 
					pass: this.config.emailPassword.value
				}
			}));
			
			let mailOptions = {
				from: this.config.emailFrom.value, 
				to: this.config.emailTo.value, 
				subject: 'UPS package is on the move!', 
				text: message
			};
			
			transport.sendMail(mailOptions, (error, info) => {
				if(error){
					super.log('Error: ' + error);
				}else{
					super.log('Message sent: ' + info.response);
				};
			});
		}
		
		action() {
			if(this.config.url.value == '') { // TO DO - move to seperate function or better yet, automate it
				super.log('No tracking url specified. Please edit configuration.');
				return;
			}
			if(this.config.emailFrom.value == '') {
				super.log('No Email From address specified. Please edit configuration.');
				return;
			}
			if(this.config.emailTo.value == '') {
				super.log('No Email To address specified. Please edit configuration.');
				return;
			}
			if(this.config.emailPassword.value == '') {
				super.log('No Email password specified. Please edit configuration.');
				return;
			}
			
			super.log('Checking package: ' + this.config.url.value);
			
			let body = this.getBody(this.config.url.value);
			body.then(($) => {
				let isFirstRun = this.previousTrackingRecords == 0 || this.previousTrackingRecords == undefined;
				
				let rows = $('table.dataTable').find('tr');
				let trackingRecords = rows.length - 1; // first row is the header
				
				if(trackingRecords != this.previousTrackingRecords) {
					console.log(`Found tracking changes. Previous count: ${this.previousTrackingRecords}. New count: ${trackingRecords}.`);
					this.previousTrackingRecords = trackingRecords;
					
					let row = $(rows[1]);
					let result = {
						location: this.getCellValue($, row, 0),
						date: this.getCellValue($, row, 1),
						time: this.getCellValue($, row, 2),
						activity: this.getCellValue($, row, 3)
					};
					
					if(isFirstRun == false)
						this.alert(result);
				}
				else
					console.log(`No change found. Still showing ${trackingRecords} tracking records.`);
			})
			.catch((reason) => {
				super.log('error: ' + reason)
			});
		}		
	}
	
	exports.UPSTracker = UPSTracker;
}

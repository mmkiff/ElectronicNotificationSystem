'use strict';
{
	let fs = require('fs');
	
	let weather = require('./Notifications/LEDs/Weather');
	let temperature = require('./Notifications/RGB/Temperature');
	let stockWatcher = require('./Notifications/RGB/StockWatcher');
	let temperatureMatrix = require('./Notifications/Matrix/Temperature');
	let webControlled = require('./Notifications/RGB/WebControlled');
	let upsTracker = require('./Notifications/Background/UPSTracker');
	let testBlinker = require('./Notifications/RGB/TestBlinker');
	let testColorChanger = require('./Notifications/RGB/TestColorChanger');
	
	class NotificationSystem {
		constructor(electronics, webUI) {
			this.electronics = electronics;
			this.webUI = webUI;			
			this.ledNotification = null;
			this.rgb1Notification = null;
			this.rgb2Notification = null;
			this.matrixNotification = null;
			this.backgroundNotification = null;
			
			this.loadConfig();
			
			webUI.messageReceivedEvent = (message) => { return this.messageReceivedFromWeb(message); }; // is this really the best way to be doing this??
		}
		
		start() {
			// start happens as the modules are loaded now.
		}
		
		messageReceivedFromWeb(message) {
			let result = null;
			
			if(message.to == 'SYSTEM') {
				if(message.key == 'SETRGB1') 
					this.setRGBModule(this.electronics.rgb1, 1, message.value);
				else if(message.key == 'SETRGB2')
					this.setRGBModule(this.electronics.rgb2, 2, message.value);
				else if(message.key == 'SETLEDs')
					this.setLEDModule(message.value);
				else if(message.key == 'SETMatrix')
					this.setMatrixModule(message.value);
				else if(message.key == 'SETBackground')
					this.setLEDModule(message.value);
				
				this.saveConfig();
				
				result = {};
			}
			else {
				if(this.ledNotification != null)
					result = this.ledNotification.messageReceivedFromWeb(message);
				if(this.rgb1Notification != null && result == null)
					result = this.rgb1Notification.messageReceivedFromWeb(message);
				if(this.rgb2Notification != null && result == null)
					result = this.rgb2Notification.messageReceivedFromWeb(message);
				if(this.matrixNotification != null && result == null)
					result = this.matrixNotification.messageReceivedFromWeb(message);
				if(this.backgroundNotification != null && result == null)
					result = this.backgroundNotification.messageReceivedFromWeb(message);
			}			
			
			return result;
		}
		
		setRGBModule(led, id, name) {			
			if(id == 1 && this.rgb1Notification != null)
				this.rgb1Notification.stop();
			else if(id == 2 && this.rgb2Notification != null)
				this.rgb2Notification.stop();
			
			let component = null;
			
			if(name == 'Blinker')
				component = new testBlinker.TestBlinker(led, id, this.webUI);
			else if(name == 'Color Changer')
				component = new testColorChanger.TestColorChanger(led, id, this.webUI);
			else if(name == 'Stock Watcher')
				component = new stockWatcher.StockWatcher(led, id, this.webUI);
			else if(name == 'Temperature')
				component = new temperature.Temperature(led, id, this.webUI);
			else if(name == 'Web Controlled')
				component = new webControlled.WebControlled(led, id, this.webUI);
			else if(name == '')
				console.log('Turning off RGB' + id); // TO DO - log for real
			else
				console.log('Unknown system: ' + message.value); // TO DO - log for real
			
			console.log('setting component for rgb' + id);
			
			if(id == 1)
				this.rgb1Notification = component;
			else if(id == 2)
				this.rgb2Notification = component;
			
			if(id == 1 && this.rgb1Notification != null)
				this.rgb1Notification.start();
			else if(id == 2 && this.rgb2Notification != null)
				this.rgb2Notification.start();
		}
		
		setLEDModule(name) {
			if(this.ledNotification != null)
				this.ledNotification.stop();
			
			if(name == 'Weather')
				this.ledNotification = new weather.Weather(this.electronics.led1, this.electronics.led2, this.electronics.led3, this.electronics.led4, this.webUI);
			else if(name == '') {
				console.log('Turning off LED'); // TO DO - log for real
				this.ledNotification = null;
			}
			else
				console.log('Unknown system: ' + message.value); // TO DO - log for real
			
			if(this.ledNotification != null)
				this.ledNotification.start();
		}
		
		setMatrixModule(name) {
			if(this.matrixNotification != null)
				this.matrixNotification.stop();
			
			if(name == 'Temperature')
				this.matrixNotification = new temperatureMatrix.Temperature(this.electronics.matrix, this.webUI);
			else if(name == '') {
				console.log('Turning off Matrix'); // TO DO - log for real
				this.matrixNotification = null;
			}
			else
				console.log('Unknown system: ' + message.value); // TO DO - log for real
			
			if(this.matrixNotification != null)
				this.matrixNotification.start();
		}
		
		setBackgroundModule(name) {
			if(this.backgroundNotification != null)
				this.backgroundNotification.stop();
			
			if(name == 'UPS Tracker')
				this.backgroundNotification = new upsTracker.UPSTracker(this.webUI);
			else if(name == '') {
				console.log('Turning off Matrix'); // TO DO - log for real
				this.backgroundNotification = null;
			}
			else
				console.log('Unknown system: ' + message.value); // TO DO - log for real
			
			if(this.backgroundNotification != null)
				this.backgroundNotification.start();
		}
		
		getConfigLocation() {
			return `./Notifications/modules.config`; 
		}
		
		saveConfig() {
			let data = {
				ledNotificationName: '',
				rgb1NotificationName: '',
				rgb2NotificationName: '',
				matrixNotificationName: '',
				backgroundNotificationName: ''
			};
			
			if(this.ledNotification != null)
				data.ledNotificationName = this.ledNotification.name;
			if(this.rgb1Notification != null)
				data.rgb1NotificationName = this.rgb1Notification.name;
			if(this.rgb2Notification != null)
				data.rgb2NotificationName = this.rgb2Notification.name;
			if(this.matrixNotification != null)
				data.matrixNotificationName = this.matrixNotification.name;
			if(this.backgroundNotification != null)
				data.backgroundNotificationName = this.backgroundNotification.name;
			
			fs.writeFileSync(this.getConfigLocation(), JSON.stringify(data), 'utf-8');
		}
		
		loadConfig() {
			if(fs.existsSync(this.getConfigLocation())) {
				let data = fs.readFileSync(this.getConfigLocation(), 'utf-8');
				data = JSON.parse(data);
				
				this.setLEDModule(data.ledNotificationName);
				this.setRGBModule(this.electronics.rgb1, 1, data.rgb1NotificationName);
				this.setRGBModule(this.electronics.rgb2, 2, data.rgb2NotificationName);
				this.setMatrixModule(data.matrixNotificationName);
				this.setBackgroundModule(data.backgroundNotificationName);			
			}
		}
	}
	
	exports.NotificationSystem = NotificationSystem;
}
# Electronic Notification System

A multipurpose notification system made up of the following:

* Arduino controlled hardware, using johnny-five:
  * 4 blue LEDs
  * 2 RGB LEDs
  * 8x8 red LED Matrix
* Swappable monitoring system for each component using node
* Web UI for monitoring logs/status using JQuery and socket.io
* Swappable paper inserts for easily switching notification output

Hardware with no insert:

![No Insert](https://gitlab.com/mmkiff/ElectronicNotificationSystem/raw/master/Images/20161116%20-%20no%20insert.jpg)


Weather insert:

![Weather Insert](https://gitlab.com/mmkiff/ElectronicNotificationSystem/raw/master/Images/20161116%20-%20weather.jpg)

Wires:

![Wires](https://gitlab.com/mmkiff/ElectronicNotificationSystem/raw/master/Images/20161116%20-%20wires.jpg)

### Setup:

* Rename 'Config - Template.js' to 'Config.js' and fill in the values
* Run 'node App.js'

### TO DO:

* Move the logging somewhere else
* Rename the webui/server
* Make configuration easier/automatic

### Future Enhancement Ideas:

* Switch to using Angular for the UI
* Make it so you can configure monitoring settings on the web ui